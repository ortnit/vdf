<?php

namespace Ortnit\Lib\Parser;

class VdfParser {

    protected static $whitespaces = [
        " ",
        "\r",
        "\n",
        "\t"
    ];

    protected $data = '';
    protected $values = [];
    public $debug = false;

    public function __construct($data) {
        $this->data = $data;
    }

    public function parse() {
        $this->data = self::trim($this->data);
        //var_dump($this->data);
        while (strlen($this->data) > 0) {
            $key = $this->_key();
            $this->values[$key] = $this->_value();
            break;
        }
        //var_dump($this->values);
        return $this->values;
    }

    protected function _key() {
        $this->_filterComment();
        $key = $this->_readData();
        $this->_debug('key => ' . $key);
        return $key;
    }

    protected function _value() {
        $value = null;
        $this->_filterComment();

        if(substr($this->data, 0, 1) == '{') {
            $this->data = substr($this->data, 1);
            $value = $this->_levelUp();
        } else {
            $value = $this->_readData();
            $this->_debug('value => ' . $value);
        }

        return $value;
    }

    protected function _levelUp () {
        $values = [];
        $this->_debug('level up');
        while(strlen($this->data) > 0) {
            $this->data = self::trim($this->data);
            $key = $this->_key();
            $values[$key] = $this->_value();
            $this->data = self::trim($this->data);
            if(substr($this->data, 0, 1) == '}') {
                $this->data = substr($this->data, 1);
                $this->_debug('level down');
                break;
            }
        }
        return $values;
    }

    protected function _filterComment() {
        $this->data = self::trim($this->data);
    }

    protected function _readData() {
        $this->data = self::trim($this->data);
        $key = '';
        if (substr($this->data, 0, 1) == "\"") {
            $this->data = substr($this->data, 1);
            $pos = 0;
            while(strlen($this->data) > 0) {
                $pos = strpos($this->data,"\"", $pos);
                if($pos === false) {
                    throw new \Exception('missing \"');
                }

                $sequence = substr($this->data, $pos-1, 2);
                if($sequence != "\\\"") {
                    break;
                }
                $pos ++;
            }
            $key = substr($this->data, 0, $pos);
            $this->data = substr($this->data, $pos + 1);
            //var_dump($pos, $this->data, $key);
        } else {
            $this->_debug('non quoted');
            for($i = 0; $i < strlen($this->data); $i++) {
                $letter = substr($this->data, $i, 1);
                if(array_search($letter, self::$whitespaces)) {
                    $key = substr($this->data, 0, $i);
                    $this->data = substr($this->data, $i);
                    break;
                }
            }
        }
        return $key;
    }

    protected function _debug($message) {
        if($this->debug) {
            echo $message . "\n";
        }
    }

    public static function trim($str) {
        return trim($str, implode('', self::$whitespaces));
    }
}
