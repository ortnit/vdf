# vdf

VDF-Parser for the Steam key value file format, used to store resources, scripts and more.
https://developer.valvesoftware.com/wiki/KeyValues#File_Format

## Author

* Tobias Spigner (tobias@spigner.de)


