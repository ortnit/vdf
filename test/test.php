<?php
/**
 * Created by PhpStorm.
 * User: tobi
<<<<<<< HEAD
 * Date: 12.03.2016
 * Time: 11:51
 */


class VdfParser {

    protected static $whitespaces = [
        " ",
        "\r",
        "\n",
        "\t"
    ];

    protected $data = '';

    protected $values = [];

    public function __construct($data) {
        $this->data = $data;
    }

    public static function trim($str) {
        return trim($str, implode('', self::$whitespaces));
    }


    public function parse() {
        $this->data = self::trim($this->data);
        //var_dump($this->data);
        while (strlen($this->data) > 0) {
            $key = $this->_key();
            $this->values[$key] = $this->_value();
            break;
        }
        //var_dump($this->values);
        return $this->values;
    }

    protected function _key() {
        $this->_filterComment();
        $key = $this->_readData();
        echo "key => " . $key . "\n";
        return $key;
    }

    protected function _value() {
        $value = null;
        $this->_filterComment();

        if(substr($this->data, 0, 1) == '{') {
            $this->data = substr($this->data, 1);
            $value = $this->_levelUp();
        } else {
             $value = $this->_readData();
            echo "value => " . $value . "\n";
        }

        return $value;
    }

    protected function _levelUp () {
        $values = [];
        echo "level up\n";
        while(strlen($this->data) > 0) {
            $this->data = self::trim($this->data);
            $key = $this->_key();
            $values[$key] = $this->_value();
            $this->data = self::trim($this->data);
            if(substr($this->data, 0, 1) == '}') {
                $this->data = substr($this->data, 1);
                echo "level down\n";
                break;
            }
        }
        return $values;
    }

    protected function _filterComment() {
        $this->data = self::trim($this->data);
    }

    protected function _readData() {
        $this->data = self::trim($this->data);
        $key = '';
        if (substr($this->data, 0, 1) == "\"") {
            $this->data = substr($this->data, 1);
            $pos = 0;
            while(strlen($this->data) > 0) {
                $pos = strpos($this->data,"\"", $pos);
                if($pos === false) {
                    throw new \Exception('missing \"');
                }

                $sequence = substr($this->data, $pos-1, 2);
                if($sequence != "\\\"") {
                    break;
                }
                $pos ++;
            }
            $key = substr($this->data, 0, $pos);
            $this->data = substr($this->data, $pos + 1);
            //var_dump($pos, $this->data, $key);
        } else {
            echo "non quoted\n";
            for($i = 0; $i < strlen($this->data); $i++) {
                $letter = substr($this->data, $i, 1);
                if(array_search($letter, self::$whitespaces)) {
                    $key = substr($this->data, 0, $i);
                    $this->data = substr($this->data, $i);
                    break;
                }
            }
        }
        return $key;
    }
}

$fileName = 'test.vdf';

$dirName = dirname(__FILE__);
$fileName = $dirName . DIRECTORY_SEPARATOR . $fileName;



$content = file_get_contents($fileName);
var_dump($content);

//escape sequence test
//function um alles unnötige wegzuschneiden


$vdf = new VdfParser($content);
$start = microtime(true);
$data = $vdf->parse();
$stop = microtime(true);



var_dump($data);

echo "time elapsed " . ($stop - $start) . " seconds.\n";
